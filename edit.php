<?php

// si se ejecuto el update escondido

if (isset($_GET['id'])) {
    require "lib/Database.php";
    require "models/Auto.php";
    // conectamos la base de datos
    // e intanciamos la clase Auto
    $obj_Auto = new Auto();
    $obj_Auto->makeConnection();


    // obtenemos la fila donde se hizo click al UPDATE

    $results = $obj_Auto->getSingleAutos($_GET["id"]);

    $id_auto = $_GET["id"];
    $make =  $results->getMake();
    $year = $results->getYear();
    $mileage = $results->getMileage();
}
?>
<?php

require "vistas/editview.php";
?>

<?php


if (isset($_POST['update'])) {

    
    $obj_Auto->setMake($_POST['marca']);
    $obj_Auto->setYear($_POST['compra']);
    $obj_Auto->setMileage($_POST['kilometraje']);
    $obj_Auto->updateAuto($obj_Auto, $id_auto);


    $_SESSION["success"] = "Registro Modificado.";
    header("Location: autos.php");
    return;
}
if (isset($_POST['cancelar'])) {
    header("Location: autos.php");
}
?>