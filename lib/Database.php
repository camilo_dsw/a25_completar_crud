<?php

require "config/config.php";

class Database{

private $host = DB_HOST;

private $user = DB_USER;

private $pass = DB_PASS;

private $dbname = DB_NAME;

private $port = DB_PORT;

private $dbh; 

private $stmt;

private $error;

// constructor

public function __construct()

{

 $dsn = 'mysql:host='.$this->host.'; port='.$this->port.'; dbname='.$this->dbname;

 $options = [


 PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

 PDO::ATTR_PERSISTENT => true, 

 PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION

 ];




 try {

 $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
 
 } catch (PDOException $e) {

 $this->error = $e->getMessage();

 echo $this->error;

 }

}

// funcion que se encarga de preparar las consultas.

public function query (string $SQL){

    $this->stmt = $this->dbh->prepare($SQL);
   
   }
   

   // funcion que se encarga de asignar los valores a los parámetros de la consulta preparada.
   public function bind($param, $value, $type=null) {

    if (is_null($type)) {
   
    switch(true) {
   
    case is_int($value):
   
    $type = PDO::PARAM_INT;
   
    break;
   
    case is_bool($value):
   
    $type = PDO::PARAM_BOOL;
   
    break;
   
    case is_null($value):
   
    $type = PDO::PARAM_NULL;
   
    break;
   
    default:
   
    $type = PDO::PARAM_STR;
   
    }
   
    }
   
    $this->stmt->bindValue($param, $value, $type);
   
   }

// este método ejecuta la consulta que query ha preparado
   public function execute() {

    return $this->stmt->execute();
   
   }

   // método para obtener todos las filas de la tabla autos
   public function resultSet() {

    $this->execute();
   
    return $this->stmt->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,"Auto");
    
   
   }

   public function single($id) 
   {
   $this->execute();
   //se llama al constructor de la clase antes de que las proiedades 
   //sean asignadas desde los valores de la columna respectiva.
   $this->stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $id);
   return $this->stmt->fetch();
   }

}
