<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>del</title>
    <link rel="stylesheet" href="estilos2.css">
</head>
<body>
<div class="container">
    <form action="" method="post">


        <?php


        ?>
        <h1>
            Eliminar Autos
        </h1>
        <div class="name">
            <div>
                <label for="marca">Marca</label><br>
                <input type="text" name="marca" value='<?php echo $make;  ?>'>
            </div>
            <br>
            <div>
                <label for="compra">Año de compra</label><br>
                <input type="number" name="compra" value='<?php echo $year;  ?>'>
            </div>
            <br>
            <div>
                <label for="kilometraje">Kilometraje</label><br>
                <input type="number" name="kilometraje" value='<?php echo $mileage;  ?>'>
            </div>
        </div>
        <br>
        <div class="btns">
            <input type="submit" value="Eliminar" id="eliminar" name="eliminar" />
            <input type="submit" value="Cancelar" id="cancelar" name="cancelar" />
        </div>
    </form>
</div>
</body>
</html>

