<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login.php</title>

    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <section class="contenedor">
        <article class="item1">

           
            <?php
            // si a pasado un suceso lo mostramos
            if (isset($_SESSION["success"])) {
                echo ('<p style="color:blue">' . htmlentities($_SESSION["success"]) . "</p>\n");
                // eliminamos los valores que tenga la super variable $_SESSION[]
                unset($_SESSION["success"]);
            }
         
            ?>


            <form method="post">

                <h3>Inserta una nueva marca</h3>
                </br>
                <p>Nombre marca</p>
                <!-- Los campos son obligatorios -->
                <input type="text" id="marca" name="make" />
                </br>
                <p>Año de compra</p>
                <!-- Los campos son obligatorios -->
                <input type="text" id="ano" name="year" />
                </br>
                <p>Kilometraje</p>
                <input type="text" id="kilome" name="milage" />
                </br>
                </br>
                <input type="submit" value="Insertar" name="insertar" />
                </br>
                </br>
                <p><a href="logout.php">Log Out</a></p>

            </form>
        </article>
        <article class="item2">
            <?php

            // mostramos los registros de la base de datos

            $results = $obj_Auto->getAutos();
            

            if (!empty($results)) {
                foreach ($results as $result) {


                    echo "<div class = 'lista'><tr>
                    <td> <b>" . $result->auto_id . "</b></td>
                    <td><b>" . $result->getMake() . "</b></td>
                    <td><b>" . $result->getYear() . "</b></td>
                    <td><b>" . $result->getMileage() . "</b></td>
                    </tr>
                   </div>

                   <div class = 'borrar'>
                    <tr><td> <form method='post'>        
                    <input  type='Submit' name='id1' value ='Borrar' />
                    <input  type='Submit' name='id2' value ='Update' />
                    <input type='hidden' name ='id'  value='$result->auto_id'/>
                    </form>
                    </td></tr>
                    </div>";
                }
                
            } else {

                $_SESSION["success"] = "No se encontraron filas.";
            }

            ?>
        </article>
    </section>
</body>
</div>

</html>