<?php

session_start();

// COMPROBAMOS SI LA VARIABLE $_SESSION[] ESTA DEFINIDA Y NO ES NULL

if (isset($_SESSION['email'])) {

  if ($_SESSION['email'] === 'camilo@debiandsw.com') {
    // COMPROBAMOS SI EL USUARIO ESTÁ LOGUEADO
    // SI LO ESTÁ INSTANCIAMOS UN OBJETO DE LA CLASE Auto() y creamos una nueva 
    // conexión con la base de datos
    //unset($_SESSION["email"]);
    require "lib/Database.php";
    require "models/Auto.php";
    
    $obj_Auto = new Auto();
    $obj_Auto->makeConnection();
  } else {
    // SI NO TERMINAMOS EL PROGRAMA
    die("ACCESS DENIED");
  }
} else {
  die("ACCESS DENIED");
}



// comprobamos si se ha definido la variable $_POST[] del formulario donde
// tenemos el botón borrar para eliminar un registro de la base de datos.
// llamamos la función delAuto()

if (isset($_POST['id1'])) 
{

  /*$obj_Auto->delAuto($_POST['id']);

  $_SESSION["success"] = "Registro eliminado.";
      header('Location: autos.php');
      return;*/
      header('location: del.php?id=' . urlencode($_POST['id'])) and die(); 
}

if (isset($_POST['id2']))
 {
  header('location: edit.php?id=' . urlencode($_POST['id'])) and die(); 
}

// comprobamos si están definidos, si no están vacios los campos marca, año y kilometraje
// y si son numéricos los campos año y kilometraje
if (isset($_POST['insertar'])) {


  if (strlen($_POST['make']) > 0 && strlen($_POST['year']) > 0 && strlen($_POST['milage']) > 0) {
    if (is_numeric($_POST['year']) && is_numeric($_POST['milage'])) {

       // añadimos un mensaje para mostrar que se ha añadido un registro a la base de datos.
      $_SESSION["success"] = "Registro insertado.";
      header('Location: autos.php');
      

      $obj_Auto->setMake($_POST['make']);
      $obj_Auto->setYear($_POST['year']);
      $obj_Auto->setMileage($_POST['milage']);
      $obj_Auto->addAuto($obj_Auto);
      return;
     
     
    } else {

      $_SESSION["success"] = "Kilometraje y año deben ser numéricos";
      header('Location: autos.php');
      return;
    }
  } else {

    $_SESSION["success"] = "Ninguno de los campos pueden estar vacios.";
    header('Location: autos.php');
    return;
  }
}



?>

<?php

require "vistas/autosview.php";
?>

