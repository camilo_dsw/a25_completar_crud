<?php

// si se ejecuto el update escondido

if (isset($_GET['id'])) {
    require "lib/Database.php";
    require "models/Auto.php";
    // conectamos la base de datos
    // e intanciamos la clase Auto
    $obj_Auto = new Auto();
    $obj_Auto->makeConnection();


    // obtenemos la fila donde se hizo click al UPDATE

    $results = $obj_Auto->getSingleAutos($_GET["id"]);

    $id_auto = $_GET["id"];
    $make =  $results->getMake();
    $year = $results->getYear();
    $mileage = $results->getMileage();
}
?>
<?php

require "vistas/delview.php";
?>

<?php


if (isset($_POST['eliminar'])) {

        
    $obj_Auto->delAuto($id_auto);


    $_SESSION["success"] = "Registro Eliminado.";
    header("Location: autos.php");
    return;
}
if (isset($_POST['cancelar'])) {
    header("Location: autos.php");
}
?>